
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2019 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f1xx_hal.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* USER CODE BEGIN Includes */
// #include <stdio.h>
  #include "string.h" // ��� ��� ������� strlen()
#define setings_flash 0x0800FC00
#define flash_adr 0x0800FC00
#define empty 0xFFFF
#define get_page 0
#define set_scale 1
///*********************************
#define PAGE_END 0x64
#define WRITE_FLASH_COUNT 0x2328
///********************************

/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/
int32_t capture=0;
uint32_t capture_old=0;
uint8_t flagBT = 0;
uint8_t flagBT_chan = 0;
uint8_t scount = 0;
uint8_t swFlag= 0;
uint16_t val = 0;
uint16_t val2 = 0;
uint16_t val2_old = 0;
uint8_t r202_out[8]={0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff}; 
uint16_t RealVoltage=100;
char trans_str[64] = {0,};
uint32_t flag_write=0;
uint16_t flash_count=0;
uint32_t real_flash=0;
uint16_t Page=0x41;

uint16_t tmp=0x41;
uint8_t swichFlag=1;

////SAVE SETINGS
uint16_t SCALE =7;

uint8_t sincBT=0;

/////


/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
void RestoreSt(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/

/* USER CODE END PFP */

/* USER CODE BEGIN 0 */


uint32_t FLASH_Read(uint32_t address)
{
    return (*(__IO uint32_t*)address);
}

uint32_t R_settings(uint32_t id)
{  
  if(id==0)
  {
  return (*(__IO uint32_t*)setings_flash);//PAGE
  }
  if(id==1)
  {
    
     return (*(__IO uint32_t*)(setings_flash+0x8));//SCALE
  }
}

void WR_Settings(uint8_t Save_parametr,uint16_t Data)
{
        FLASH_EraseInitTypeDef Flash_erase;
	HAL_StatusTypeDef Return_erase=NULL;
	HAL_StatusTypeDef	flash_ok = HAL_ERROR;
	
	uint32_t Error=0;
	
	Flash_erase.TypeErase=FLASH_TYPEERASE_PAGES;
	Flash_erase.PageAddress=flash_adr;
	Flash_erase.Banks=FLASH_BANK_1;
	Flash_erase.NbPages=1;

	// erase sector and write ystavok, bank 127 c adress
    // 0x0801FC00 - 0x0801FFFF
    flash_ok = HAL_ERROR;
    while(flash_ok != HAL_OK){
    	flash_ok = HAL_FLASH_Unlock();
    }
    
    if (HAL_FLASHEx_Erase(&Flash_erase, &Error) != HAL_OK) 
    {     
    	Return_erase=HAL_FLASH_Lock();
    }
    
    //PAGE FLASH
    if (Save_parametr==0)
    {
    flash_ok = HAL_ERROR;
    while(flash_ok != HAL_OK){
    	flash_ok = HAL_FLASH_Program(FLASH_TYPEPROGRAM_HALFWORD, setings_flash, Data);
                             }
    }
    
    //SCALE
    if (Save_parametr==1)
    {
    flash_ok = HAL_ERROR;
    while(flash_ok != HAL_OK){
    	flash_ok = HAL_FLASH_Program(FLASH_TYPEPROGRAM_HALFWORD, setings_flash+0x8, Data);
                             }
    }
   
 
    flash_ok = HAL_ERROR;
    while(flash_ok != HAL_OK){
    	flash_ok = HAL_FLASH_Lock();
    }

}

void fw_cicle()
{
         Page= R_settings(get_page);
          if(Page>PAGE_END){Page=0x41;}
       //  if(Page==empty){ Page=0x41;} //65
        // if(Page>0x46 ){ Page=0x41;}  
        // if(Page<0x41 ){ Page=0x41;}  
       
          flash_count++;
          if(flash_count>WRITE_FLASH_COUNT)// ne zapisali li 9000????
           {
             Page++;
             WR_Settings(get_page,Page);
             flash_count=0;
             if(Page>PAGE_END){Page=0x41;}
             
           }
        real_flash= 0x08000000+((0x400)*  Page); // uznali kakaya flash ispolzovat//get real flash

        FLASH_EraseInitTypeDef Flash_erase;
	HAL_StatusTypeDef Return_erase=NULL;
	HAL_StatusTypeDef	flash_ok = HAL_ERROR;
	
	uint32_t Error=0;

	
	Flash_erase.TypeErase=FLASH_TYPEERASE_PAGES;
	Flash_erase.PageAddress=real_flash;
	Flash_erase.Banks=FLASH_BANK_1;
	Flash_erase.NbPages=1;
        
    flash_ok = HAL_ERROR;
    while(flash_ok != HAL_OK){
    	flash_ok = HAL_FLASH_Unlock();
    }
    
    if (HAL_FLASHEx_Erase(&Flash_erase, &Error) != HAL_OK) 
    {     
    	Return_erase=HAL_FLASH_Lock();
    }
    
    // ZNACHENIE YARKOSTI
    flash_ok = HAL_ERROR;
    while(flash_ok != HAL_OK){
    	flash_ok = HAL_FLASH_Program(FLASH_TYPEPROGRAM_HALFWORD, real_flash, capture);
    }
    
    // COUNTER WR FLASH
    flash_ok = HAL_ERROR;
    while(flash_ok != HAL_OK){
    	flash_ok = HAL_FLASH_Program(FLASH_TYPEPROGRAM_HALFWORD, (real_flash+0x8), flash_count);
    }
    
       
    
    flash_ok = HAL_ERROR;
    while(flash_ok != HAL_OK){
    	flash_ok = HAL_FLASH_Lock();
    }
   
           
          
         
}



 

void RestoreSt()
{
         Page= R_settings(get_page); 
         SCALE=R_settings( set_scale);
         if(Page>PAGE_END){Page=0x41;}
         real_flash= 0x08000000+((0x400)*  Page); // uznali kakaya flash ispolzovat
         flash_count=FLASH_Read(real_flash+0x8);
         TIM4->CNT= FLASH_Read(real_flash);
         if(flash_count==empty)
         {
          flash_count=0 ;
         }
}


void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
        if(htim->Instance == TIM2) //check if the interrupt comes from TIM3
        {
         
            
             LED_ON_OFF;
             scount++;
             val=abs(capture-capture_old);
             
               if( val>4)
               { 
             printf("VALLL:%d \r\n",val);
             scount=0;
             flagBT=0;
               }
             
              capture_old=capture;
           
             
             if(scount>10){scount=10;}
             if(scount>5)
             {
               flagBT=1;
             }
             
             ///save resurs flash/ zapis tolko izmeneniya
           
             
             
              if (flagBT==0)
               {
                   flag_write++;
                   if(flag_write>10)
                   {
                   
                     fw_cicle();
                   flag_write=5;
                   printf("flashWrite!\r\n");
                   }
               }
             

          
        }
}

void KLIK_KLACK(uint8_t click)
{
 
if(click==1){printf("1 \r\n");HAL_UART_Transmit(&huart2,  r202_out, 6,1000);TIM4->CNT=293;}
if(click==2){printf("2 \r\n");HAL_UART_Transmit(&huart2,  r202_out, 6,1000);TIM4->CNT=180;}
if(click==3){printf("3 \r\n");HAL_UART_Transmit(&huart2,  r202_out, 6,1000);TIM4->CNT=100;}
if(click==4){printf("4 \r\n");HAL_UART_Transmit(&huart2,  r202_out, 6,1000);TIM4->CNT=400;}

}

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  *
  * @retval None
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_TIM1_Init();
  MX_USART2_UART_Init();
  MX_TIM2_Init();
  MX_TIM4_Init();
  /* USER CODE BEGIN 2 */
 HAL_TIM_Base_Start_IT(&htim2);
HAL_TIM_PWM_Start_IT(&htim1, TIM_CHANNEL_3);
HAL_TIM_Encoder_Start(&htim4, TIM_CHANNEL_ALL);
//





  
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */

  RestoreSt();//restore flash; 
   while (1)
  {
           
swFlag= HAL_GPIO_ReadPin(GPIOB, switch_Pin);
if(swFlag==0)
{
if(swichFlag==1){KLIK_KLACK(swichFlag);}
if(swichFlag==2){KLIK_KLACK(swichFlag);}
if(swichFlag==3){KLIK_KLACK(swichFlag);}
if(swichFlag==4){KLIK_KLACK(swichFlag);}
   HAL_Delay(2000);
   swichFlag++;
   if(swichFlag>4){swichFlag=1;}
}
    capture = TIM4->CNT;
    if(( capture>500)&&(capture<550)){TIM4->CNT=500;capture=500;}
    if( capture>1000){TIM4->CNT=0;capture=0;}
    
  
  
  if(flagBT==0){
    RealVoltage=capture*SCALE;
    
    
    TIM1->CCR3= RealVoltage;
   
  
    printf("VT:%d  \r\n",RealVoltage);
    
    snprintf(trans_str, 63, "Count: %d \r\n", RealVoltage);
    HAL_UART_Transmit(&huart2, (uint8_t*)trans_str, strlen(trans_str), 1000);
   
  }
 // if(capture<2){flagBT=0;}
  
  
 HAL_UART_Receive(&huart2,  r202_out, 8,100); 
  
if( (r202_out[0]==0x61)&&(r202_out[1]==0x74)&&(r202_out[2]==0x3D))	//at=	
{

r202_out[0]=0x2b;
if(r202_out[3]==0x30){printf("0 \r\n");HAL_UART_Transmit(&huart2,  r202_out, 6,1000);TIM4->CNT=20;}
if(r202_out[3]==0x31){printf("1 \r\n");HAL_UART_Transmit(&huart2,  r202_out, 6,1000);TIM4->CNT=40;}
if(r202_out[3]==0x32){printf("2 \r\n");HAL_UART_Transmit(&huart2,  r202_out, 6,1000);TIM4->CNT=70;}
if(r202_out[3]==0x33){printf("3 \r\n");HAL_UART_Transmit(&huart2,  r202_out, 6,1000);TIM4->CNT=100;}
if(r202_out[3]==0x34){printf("4 \r\n");HAL_UART_Transmit(&huart2,  r202_out, 6,1000);TIM4->CNT=200;}
if(r202_out[3]==0x35){printf("5 \r\n");HAL_UART_Transmit(&huart2,  r202_out, 6,1000);TIM4->CNT=300;}
if(r202_out[3]==0x36){printf("6 \r\n");HAL_UART_Transmit(&huart2,  r202_out, 6,1000);TIM4->CNT=350;}
if(r202_out[3]==0x37){printf("7 \r\n");HAL_UART_Transmit(&huart2,  r202_out, 6,1000);TIM4->CNT=400;}
if(r202_out[3]==0x38){printf("8 \r\n");HAL_UART_Transmit(&huart2,  r202_out, 6,1000);TIM4->CNT=450;}
if(r202_out[3]==0x39){printf("9 \r\n");HAL_UART_Transmit(&huart2,  r202_out, 6,1000);TIM4->CNT=500;}





r202_out[0]=0x44;
r202_out[1]=0x44;
r202_out[2]=0x44;
r202_out[3]=0x44;
r202_out[4]=0x44;


//flagBT = 1;
}

if( (r202_out[0]==0x73)&&(r202_out[1]==0x6b)&&(r202_out[2]==0x3D))	//sk=	
{
 r202_out[0]=0x2b; 
 
 
 
if(r202_out[3]==0x30){printf("0 \r\n");HAL_UART_Transmit(&huart2,  r202_out, 6,1000);WR_Settings(set_scale,0x2);}
if(r202_out[3]==0x31){printf("1 \r\n");HAL_UART_Transmit(&huart2,  r202_out, 6,1000);WR_Settings(set_scale,0x3);}
if(r202_out[3]==0x32){printf("2 \r\n");HAL_UART_Transmit(&huart2,  r202_out, 6,1000);WR_Settings(set_scale,0x4);}
if(r202_out[3]==0x33){printf("3 \r\n");HAL_UART_Transmit(&huart2,  r202_out, 6,1000);WR_Settings(set_scale,0x5);}
if(r202_out[3]==0x34){printf("4 \r\n");HAL_UART_Transmit(&huart2,  r202_out, 6,1000);WR_Settings(set_scale,0x6);}
if(r202_out[3]==0x35){printf("5 \r\n");HAL_UART_Transmit(&huart2,  r202_out, 6,1000);WR_Settings(set_scale,0x7);}
if(r202_out[3]==0x36){printf("6 \r\n");HAL_UART_Transmit(&huart2,  r202_out, 6,1000);WR_Settings(set_scale,0x8);}
if(r202_out[3]==0x37){printf("7 \r\n");HAL_UART_Transmit(&huart2,  r202_out, 6,1000);WR_Settings(set_scale,0x9);}
if(r202_out[3]==0x38){printf("8 \r\n");HAL_UART_Transmit(&huart2,  r202_out, 6,1000);WR_Settings(set_scale,0x10);}
if(r202_out[3]==0x39){printf("9 \r\n");HAL_UART_Transmit(&huart2,  r202_out, 6,1000);WR_Settings(set_scale,0x11);}

 

r202_out[0]=0x44;
r202_out[1]=0x44;
r202_out[2]=0x44;
r202_out[3]=0x44;
r202_out[4]=0x44;
}
       
  /* USER CODE END WHILE */

  /* USER CODE BEGIN 3 */

  }
  /* USER CODE END 3 */

}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure the Systick interrupt time 
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick 
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  file: The file name as string.
  * @param  line: The line in file as a number.
  * @retval None
  */
void _Error_Handler(char *file, int line)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  while(1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
